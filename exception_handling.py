
# The MIT License (MIT)
# Copyright (c) 2019 OctoNezd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import sentry_sdk
from pprint import pformat
import time
from datetime import datetime
import traceback
import os
import inspect
import subprocess
import settings
import logging
release = subprocess.check_output(
    'git log -n 1 --pretty="%h"', shell=True).decode('utf-8')

LOGGER = logging.getLogger("Exception Handling")
LOGGER.info("Release %s", release)
if settings.USE_SENTRY:
    sentry_sdk.init(settings.SENTRY_URL, release=release)
    LOGGER.info("Sentry initialization complete")


def catch_exc(user_context={}, tags_context={}, extra_context={}):
    tags_context["Exception Source"] = inspect.getouterframes(
        inspect.currentframe(), 2)[1][3]
    tags_context["Environment"] = settings.SENTRY_ENV
    LOGGER.error("Exception at %s",
                tags_context["Exception Source"], exc_info=True)
    if settings.USE_SENTRY:
        with sentry_sdk.configure_scope() as scope:
            scope.user = user_context
            for extra in extra_context.items():
                scope.set_extra(*extra)
            for tag in tags_context.items():
                scope.set_tag(*tag)
            return sentry_sdk.capture_exception()
    else:
        os.makedirs("plugdata/exception_logs", exist_ok=True)
        exc_code = time.time()
        with open("plugdata/exception_logs/%s.txt" % exc_code, 'w', encoding="utf-8") as io:
            io.write("Error occured at %s\n" %
                     datetime.fromtimestamp(exc_code))
            io.write("Tags:\n%s\n" % pformat(tags_context))
            io.write("Extra context:\n%s\n" % pformat(extra_context))
            io.write("User context:\n%s\n" % pformat(user_context))
            io.write("Traceback:\n%s\n" % traceback.format_exc())
            io.seek(0)
        return exc_code
